#include <AFMotor.h>
#include <Servo.h>

AF_DCMotor motor1(1);
AF_DCMotor motor2(2);                                                     
Servo servo1;


void setup()
{
  Serial.begin(9600);
  
  servo1.attach(9);
  
  motor1.setSpeed(250);
  motor2.setSpeed(250);
  
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  
  
}


void loop()
{
motor1.run(FORWARD);
motor2.run(FORWARD);
servo1.write(45);
delay(3000);

motor1.run(RELEASE);
motor2.run(RELEASE);
servo1.write(-45);
delay(3000);

motor1.run(BACKWARD);
motor2.run(BACKWARD);
servo1.write(-45);
delay(3000);





}

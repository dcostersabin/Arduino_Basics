
int trigPin = 8 ;
int echoPin = 9 ;
int ledPin = 7 ;


void setup() {

  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {

  long check_distance = distance();
  delay(10);
  if (check_distance <= 10)
  {
    ledOn();
  }
  else
  {
    ledOff();
  }


}

long distance()
{
  long distance, duration ;

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;
  return distance ;

}

void ledOn()
{
  digitalWrite(ledPin, HIGH);
}
void ledOff()
{
  digitalWrite(ledPin, LOW);
}

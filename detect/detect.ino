#include <AFMotor.h>
#include <Servo.h>

AF_DCMotor motor1(1);
AF_DCMotor motor2(2);
Servo servo1;
int trigPin = 14 ;
int echoPin = 15 ;
long current_distance;



void setup()
{
  Serial.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  servo1.write(90);
  servo1.attach(9);

  motor1.setSpeed(250);
  motor2.setSpeed(250);

  motor1.run(RELEASE);
  motor2.run(RELEASE);




}


void loop()
{
  current_distance = distance();


  if(current_distance > 30)
  {
    move_forward();
  }
  else
  {
    stop_car();
  }

}

void stop_car()
{
  Serial.print("stop");
  motor1.run(RELEASE); 
  motor2.run(RELEASE);


  long left , right ;

  left = left_distance();
  right = right_distance();

  if( left > right)
  {
    move_right();
  }
  else if(right > left)
  {
    move_left(); 
  }
  else
  {
    move_back();
  }  

}


void move_left()
{
  delay(1500);
  motor2.run(FORWARD);
  delay(1000);
}

void move_right()
{
  delay(1500);
  motor1.run(FORWARD);
  delay(1000);
}

void move_back()
{
  motor1.run(BACKWARD);
  motor2.run(BACKWARD);
  delay(1000);
  stop_car();
}






long left_distance()
{
  servo1.write(45);
  long dist;
  dist = distance();
  return dist;
}

long right_distance()
{
  servo1.write(135);
  long dist;
  dist = distance();
  return dist;
}







void move_forward()
{
  delay(1500);

  Serial.println("forward");
  servo1.write(90);
  motor1.run(FORWARD);
  motor2.run(FORWARD);
  delayMicroseconds(5000);

}


long distance()
{
  Serial.println("distance");
  long distance, duration ;
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;
  Serial.println(distance);
  return distance ;

}




